'use strict'

var mongoose = require('mongoose');
var squema = mongoose.Schema;

var frutaSchema = squema({
  nombre: String,
  color: String,
  temporada: Boolean
});

module.exports = mongoose.model('Frutas', frutaSchema);
