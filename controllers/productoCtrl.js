'use strict'

var Producto = require('../models/productos')

function saveProducto(req, res) {
  var producto = new Producto();

  var params = req.body;

  if (params.nombre) {
    producto.nombre = params.nombre;
    producto.descripcion = params.descripcion;
    producto.precio = params.precio;
    producto.imagen = params.imagen;

    producto.save((err, productoStored) => {
      if (err) {
        res.status(500).send({
          message: 'Error en el servidor'
        });
      } else {
        if (productoStored) {
          res.status(200).send({
            producto: productoStored,
            status: 200
          });
        } else {
          res.status(200).send({
            message: 'No se ha guardado la producto'
          });
        }
      }
    })
  } else {
    res.status(200).send({
      message: 'El nombre del producto es obligatoirio'
    })
  }
}

function getProductos(req, res) {
  Producto.find({}).sort({
    nombre: 1
  }).exec((err, productos) => {
    if (err) {
      res.status(500).send({
        message: 'Error en el servidor'
      });
    } else {
      if (productos) {
        res.status(200).send({
          productos,
          status: 200
        });
      } else {
        res.status(404).send({
          message: 'No se obtuvieron las productos'
        });
      }
    }
  })
}

function getProducto(req, res) {
  var productoId = req.params.id;

  Producto.findById(productoId).exec((err, producto) => {
    if (err) {
      res.status(500).send({
        message: 'Error en el servidor'
      });
    } else {
      if (producto) {
        res.status(200).send({
          producto,
          status: 200
        });
      } else {
        res.status(404).send({
          message: 'No se existe la productos'
        });
      }
    }
  });
}

function updateProducto(req, res) {
  var productoId = req.params.id;
  var update = req.body;

  Producto.findByIdAndUpdate(productoId, update, {
    new: true
  }, (err, productoUpdated) => {
    if (err) {
      res.status(500).send({
        message: 'Error en el servidor'
      });
    } else {
      if (productoUpdated) {
        res.status(200).send({
          producto: productoUpdated,
          status: 200
        });
      } else {
        res.status(404).send({
          message: 'No se existe la producto'
        });
      }
    }
  })
}

function deleteProducto(req, res) {
  var productoId = req.params.id;

  Producto.findByIdAndRemove(productoId, (err, productoRemoved) => {
    if (err) {
      res.status(500).send({
        message: 'Error en el servidor'
      });
    } else {
      if (productoRemoved) {
        res.status(200).send({
          producto: productoRemoved,
          status: 200
        });
      } else {
        res.status(404).send({
          message: 'No se existe la producto'
        });
      }
    }
  })
}

module.exports = {
  saveProducto,
  getProductos,
  getProducto,
  updateProducto,
  deleteProducto
};
