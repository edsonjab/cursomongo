'use strict'

// Importamos el modulo para subir ficheros
var fs = require('fs');
var path = require('path');

function uploadImage(req, res) {

    var tmp_path = req.files[0].path;
    // Ruta donde colocaremos las imagenes
    var target_path = './images/' + req.files[0].originalname;
   // Comprobamos que el fichero es de tipo imagen
    if (req.files[0].mimetype.indexOf('image')==-1){
      res.send('El fichero que deseas subir no es una imagen');
    } else {
         // Movemos el fichero temporal tmp_path al directorio que hemos elegido en target_path
        fs.rename(tmp_path, target_path, function(err) {
            if (err) throw err;
            // Eliminamos el fichero temporal
            fs.unlink(tmp_path, function() {
                if (err) throw err;
                res.status(200).send({
                  path: '/images/' + req.files[0].originalname,
                  filename: req.files[0].originalname,
                  title: 'Imagen guardada'
                });
            });
         });
     }
}

function getImage(req, res) {
  var filename = req.params.filename;
  res.sendFile(path.resolve('./images/'+filename));
}

module.exports = {
  uploadImage,
  getImage
};
