'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

// cargar rutas
var fruta_router = require('./routes/fruta');
var producto_router = require('./routes/producto');
var upload = require('./routes/upload');


// body-parser
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

// Configurar CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, DELETE, POST");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// rutas base
app.use('/api', fruta_router);
app.use('/api', producto_router);
app.use('/api', upload);

module.exports = app;
