'use strict'

var express = require('express');
var uploadCtrl = require('../controllers/uploadCtrl');
var multer = require('multer');

var api = express.Router();

var upload = multer({dest: "../images/"}).array("uploads[]", 12);

api.get('/images/:filename', uploadCtrl.getImage);

// indicamos que del fichero upload.js haga mención a la función Uploads para subir la imagen.
api.post('/upload', upload, uploadCtrl.uploadImage);

module.exports = api;
