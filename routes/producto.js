'use strict'

var express = require('express');
var productoCtrl = require('../controllers/productoCtrl');

var api = express.Router();

api.post('/productos', productoCtrl.saveProducto);
api.get('/productos', productoCtrl.getProductos);
api.get('/productos/:id', productoCtrl.getProducto);
api.put('/productos/:id', productoCtrl.updateProducto);
api.delete('/productos/:id', productoCtrl.deleteProducto);
module.exports = api;
